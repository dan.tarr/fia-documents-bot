package utils

import (
	"crypto/sha256"
	"encoding/hex"
)

func Sha256HashToString(string string) string {
	hash := sha256.New()
	hash.Write([]byte(string))

	return hex.EncodeToString(hash.Sum(nil))
}
