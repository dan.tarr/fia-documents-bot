package utils

import (
	"fia-documents-bot/constants"
	"fia-documents-bot/structs"
)

func FindFIADocumentPage(formula int, season int) *structs.FIADocumentPage {
	for _, documentPage := range constants.FIADocumentPages {
		if documentPage.Formula == formula && documentPage.Season == season {
			return &documentPage
		}
	}

	return nil
}
