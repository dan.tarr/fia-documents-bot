package utils

func RemoveCharacterFromString(inputString string, index int) string {
	stringRune := []rune(inputString)
	stringRune = append(stringRune[0:index], stringRune[index+1:]...)

	return string(stringRune)
}
