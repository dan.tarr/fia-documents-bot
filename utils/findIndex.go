package utils

import (
	"fia-documents-bot/database/models"
)

func FindDocumentIndexByID(documents []models.Document, id string) int {
	for index, document := range documents {
		if document.ID == id {
			return index
		}
	}

	return -1
}
