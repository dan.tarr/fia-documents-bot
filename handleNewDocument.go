package main

import (
	"fmt"
	"math"
	"net/http"
	"os"
	"strings"
	"sync"

	"fia-documents-bot/aws/dynamodb"
	"fia-documents-bot/constants"
	"fia-documents-bot/database/models"
	"fia-documents-bot/logger"
	"fia-documents-bot/structs"
	"fia-documents-bot/twitter"
	"fia-documents-bot/utils"

	"github.com/gen2brain/go-fitz"
	"github.com/google/uuid"
)

func postTweet(client *http.Client, tweetText string, placeID string, mediaIDs []string, replyID string) twitter.TweetResponseV1 {
	tweet, err := twitter.CreateTweetV1(client, twitter.CreateTweetBodyV1{
		Status:            tweetText,
		MediaIDs:          strings.Join(mediaIDs[:], ","),
		InReplyToStatusID: replyID,
		PlaceID:           placeID,
	})
	if err != nil {
		logger.Critical("Failed to create Tweet\n%s", err)
		os.Exit(1)
	}

	logger.Success("Posted Tweet with %d images (Tweet ID: %s)", len(mediaIDs), tweet.IDString)

	return tweet
}

func HandleNewFIADocument(fiaDocument structs.FIADocument, eventID string, formula int, currentSeason int) {
	dynamodbClient := dynamodb.CreateClient()

	document := models.Document{
		ID:      fiaDocument.ID,
		EventID: eventID,
		Season:  currentSeason,
	}

	item, err := models.DocumentToMap(document)
	if err != nil {
		logger.Critical("Failed to convert Document to map\n%s", err)
		return
	}

	_, err = dynamodb.PutItem(dynamodbClient, item)
	if err != nil {
		logger.Critical("Failed to add document to DynamoDB\n%s", err)
		return
	}

	logger.Success("Added document to DynamoDB (Document ID: %s)", document.ID)

	tempDir := os.Getenv("TEMP_DIR")
	pdfFileName := uuid.NewString()

	err = os.MkdirAll(fmt.Sprintf("%s/%s", tempDir, document.ID), os.ModePerm)
	if err != nil {
		logger.Critical("Failed to create temp dir for document\n%s", err)
		return
	}

	err = utils.DownloadFile(fiaDocument.URL, fmt.Sprintf("%s/%s/%s.pdf", tempDir, document.ID, pdfFileName))
	if err != nil {
		logger.Critical("Failed to download FIA document PDF\n%s", err)
		return
	}

	logger.Success("Downloaded PDF to %s/%s/%s.pdf", tempDir, document.ID, pdfFileName)

	doc, err := fitz.New(fmt.Sprintf("%s/%s/%s.pdf", tempDir, document.ID, pdfFileName))
	if err != nil {
		logger.Critical("Failed to read PDF\n%s", err)
		return
	}

	var wg sync.WaitGroup

	for i := 1; i < doc.NumPage()+1; i++ {
		wg.Add(1)

		go func(i int) {
			CreatePDFImage(doc, document.ID, i)
			wg.Done()
		}(i)
	}

	wg.Wait()

	twitterCredentials := twitter.GetCredentials(formula)
	twitterHTTPClient := twitter.CreateHTTPClient(
		twitterCredentials.ConsumerKey,
		twitterCredentials.ConsumerSecret,
		twitterCredentials.Token,
		twitterCredentials.TokenSecret,
	)

	mediaIDs := make([]string, doc.NumPage())

	for i := 1; i < doc.NumPage()+1; i++ {
		wg.Add(1)

		go func(i int) {
			UploadPDFImages(twitterHTTPClient, i, &mediaIDs, document.ID)
			wg.Done()
		}(i)
	}

	wg.Wait()

	escapedURL := strings.ReplaceAll(fiaDocument.URL, " ", "%20")
	tweetText := fmt.Sprintf("New Document Published! (%s)\n\n📄 %s\n🔗 %s", fiaDocument.EventTitle, fiaDocument.Title, escapedURL)
	twitterPlaceID := constants.FIAEventTwitterPlaceIDs[fiaDocument.EventTitle]

	if len(mediaIDs) <= 4 {
		postTweet(twitterHTTPClient, tweetText, twitterPlaceID, mediaIDs, "")
		return
	}

	lastTweetID := ""
	numberOfTweets := int(math.Ceil(float64(len(mediaIDs)) / float64(4)))

	tweetText += fmt.Sprintf("\n(1/%d)", numberOfTweets)

	for i := 0; i < len(mediaIDs); i += 4 {
		tweetNumber := (i / 4) + 1
		var thisChunk = 4

		if i+4 > len(mediaIDs) {
			thisChunk = len(mediaIDs) - i
		}

		// If this is not the first tweet, change the tweet text to only include the tweet number
		if tweetNumber != 1 {
			tweetText = fmt.Sprintf("(%d/%d)", tweetNumber, numberOfTweets)
		}

		tweetMediaIDs := mediaIDs[i : i+thisChunk]
		tweet := postTweet(twitterHTTPClient, tweetText, twitterPlaceID, tweetMediaIDs, lastTweetID)

		lastTweetID = tweet.IDString
	}
}
