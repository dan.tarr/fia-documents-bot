package main

import (
	"context"
	"errors"
	"fmt"
	"os"
	"sync"
	"time"

	"fia-documents-bot/aws/dynamodb"
	"fia-documents-bot/database/models"
	"fia-documents-bot/logger"
	"fia-documents-bot/scrapers"
	"fia-documents-bot/structs"
	"fia-documents-bot/utils"
	"fia-documents-bot/version"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/expression"
	_ "github.com/joho/godotenv/autoload"
)

func handleRequest(_ context.Context, event structs.LambdaEvent) error {
	logger.Info("Starting FIA Documents Bot v%s", version.Version)

	if event.Formula == 0 {
		logger.Critical("Event contains invalid formula value")
		return errors.New("event contains invalid formula value")
	}

	dynamodbClient := dynamodb.CreateClient()

	currentSeason := time.Now().Year()
	documentPage := utils.FindFIADocumentPage(event.Formula, currentSeason)

	if documentPage == nil {
		msg := fmt.Sprintf("FIA documents URL could not be found for F%d %d", event.Formula, currentSeason)
		logger.Warning(msg)
		return errors.New(msg)
	}

	logger.Info("Fetching FIA documents")

	fiaEvents, err := scrapers.ScrapeFIADocuments(documentPage.URL, event.Formula)
	if err != nil {
		logger.Critical("Failed to scrape FIA documents %s", err)
		return err
	}

	if len(fiaEvents) == 0 {
		logger.Info("No events found")
		return err
	}

	logger.Info("Scraped documents from %d events", len(fiaEvents))

	// Old events are unlikely to get new documents added, only check the latest event's documents to avoid making a load of DynamoDB requests
	latestEvent := fiaEvents[0]

	logger.Info("Latest event is \"%s\"", latestEvent.EventTitle)

	if len(latestEvent.Documents) == 0 {
		logger.Info("No documents in the latest event")
		return err
	}

	keyCondition := expression.KeyEqual(expression.Key("EventID"), expression.Value(latestEvent.EventID))
	keyFilter := expression.Equal(expression.Key("Season"), expression.Value(currentSeason))
	expr, err := expression.NewBuilder().WithKeyCondition(keyCondition).WithFilter(keyFilter).Build()
	if err != nil {
		logger.Critical("Error when creating DynamoDB expression\n%s", err)
		return err
	}

	queryOutput, err := dynamodb.QueryAll(dynamodbClient, expr)
	if err != nil {
		logger.Critical("Error when querying DynamoDB\n%s", err)
		return err
	}

	documents, err := models.MapsToDocuments(queryOutput)
	if err != nil {
		logger.Critical("Error when mapping documents\n%s", err)
		return err
	}

	if len(latestEvent.Documents) <= len(documents) {
		logger.Info("No new documents found")
		return err
	}

	logger.Success("Fetched %d documents from DynamoDB for event \"%s\" (Event ID: %s)", len(documents), latestEvent.EventTitle, latestEvent.EventID)

	newFIADocuments := make([]structs.FIADocument, 0)

	for _, document := range latestEvent.Documents {
		documentIndex := utils.FindDocumentIndexByID(documents, document.ID)

		if documentIndex == -1 {
			newFIADocuments = append(newFIADocuments, document)
		}
	}

	logger.Info("Found %d new documents", len(newFIADocuments))

	for i := 0; i < len(newFIADocuments); i += 2 {
		var wg sync.WaitGroup
		toLoop := 2

		if i+2 > len(newFIADocuments) {
			toLoop = len(newFIADocuments) - i
		}

		for j := 0; j < toLoop; j++ {
			wg.Add(1)

			go func(fiaDocument structs.FIADocument) {
				defer wg.Done()
				HandleNewFIADocument(fiaDocument, latestEvent.EventID, event.Formula, currentSeason)
			}(newFIADocuments[i+j])
		}

		wg.Wait()
	}

	logger.Info("Finished handling all new documents")

	return nil
}

func main() {
	_, isLambda := os.LookupEnv("AWS_LAMBDA_FUNCTION_NAME")

	if isLambda {
		lambda.Start(handleRequest)
	} else {
		// This is essentially for local testing/development
		_ = handleRequest(context.TODO(), structs.LambdaEvent{
			Formula: 1,
		})
	}
}
