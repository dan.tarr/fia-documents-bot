package dynamodb

import (
	"context"
	"log"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/aws/retry"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/expression"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
)

func CreateClient() *dynamodb.Client {
	// Increase retry count to allow the SDK to recover from exceeding DynamoDB provisioned throughput automatically
	cfg, err := config.LoadDefaultConfig(context.TODO(), config.WithRetryer(func() aws.Retryer {
		return retry.AddWithMaxAttempts(retry.NewStandard(), 10)
	}))
	if err != nil {
		log.Fatalln(err)
	}

	return dynamodb.NewFromConfig(cfg)
}

func PutItem(client *dynamodb.Client, item map[string]types.AttributeValue) (*dynamodb.PutItemOutput, error) {
	return client.PutItem(context.TODO(), &dynamodb.PutItemInput{
		TableName:              aws.String("FIADocuments"),
		Item:                   item,
		ReturnConsumedCapacity: types.ReturnConsumedCapacityIndexes,
	})
}

func BatchWriteItem(client *dynamodb.Client, requestItems map[string][]types.WriteRequest) (*dynamodb.BatchWriteItemOutput, error) {
	return client.BatchWriteItem(context.TODO(), &dynamodb.BatchWriteItemInput{
		RequestItems:                requestItems,
		ReturnConsumedCapacity:      types.ReturnConsumedCapacityIndexes,
		ReturnItemCollectionMetrics: types.ReturnItemCollectionMetricsSize,
	})
}

func Query(client *dynamodb.Client, expr expression.Expression, lastEvaluatedKey map[string]types.AttributeValue) (*dynamodb.QueryOutput, error) {
	return client.Query(context.TODO(), &dynamodb.QueryInput{
		TableName:                 aws.String("FIADocuments"),
		IndexName:                 aws.String("EventID-index"),
		Select:                    types.SelectAllProjectedAttributes,
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		ProjectionExpression:      expr.Projection(),
		KeyConditionExpression:    expr.KeyCondition(),
		ReturnConsumedCapacity:    types.ReturnConsumedCapacityIndexes,
		ExclusiveStartKey:         lastEvaluatedKey,
	})
}

func QueryAll(client *dynamodb.Client, expr expression.Expression) ([]map[string]types.AttributeValue, error) {
	hasMore := true
	var lastEvaluatedKey map[string]types.AttributeValue
	items := make([]map[string]types.AttributeValue, 0)

	for hasMore {
		queryOutput, err := Query(client, expr, lastEvaluatedKey)
		if err != nil {
			return []map[string]types.AttributeValue{}, err
		}

		items = append(items, queryOutput.Items...)

		lastEvaluatedKey = queryOutput.LastEvaluatedKey

		if len(queryOutput.LastEvaluatedKey) == 0 {
			hasMore = false
		}
	}

	return items, nil
}
