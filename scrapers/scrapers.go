package scrapers

import (
	"fmt"
	"time"

	"fia-documents-bot/structs"
	"fia-documents-bot/utils"

	"github.com/gocolly/colly"
)

func ScrapeFIADocuments(url string, formula int) ([]structs.FIAEvent, error) {
	c := colly.NewCollector(
		colly.AllowedDomains("www.fia.com"),
	)

	events := make([]structs.FIAEvent, 0)

	// Go through each of the event-wrapper elements which is the container of each race
	c.OnHTML(".event-wrapper", func(eventElement *colly.HTMLElement) {
		eventTitle := eventElement.ChildText(".event-title")

		event := structs.FIAEvent{
			EventID:    utils.Sha256HashToString(fmt.Sprintf("%s:%d", eventTitle, formula)),
			EventTitle: eventTitle,
		}

		// Go through each of the document-row elements which is a singular document for the current race/event container
		eventElement.ForEach(".document-row", func(i int, documentElement *colly.HTMLElement) {
			title := documentElement.ChildText(".title")
			documentUrl := "https://www.fia.com" + documentElement.ChildAttr("a", "href")
			publishDateString := documentElement.ChildText(".date-display-single")

			// On some documents, the FIA added a random extra period to the published date
			if len(publishDateString) == 15 {
				publishDateString = utils.RemoveCharacterFromString(publishDateString, 8)
			}

			// FIA publish times are in the CET timezone
			cetTimezone, _ := time.LoadLocation("CET")
			publishDate, _ := time.ParseInLocation("02.01.06 15:04", publishDateString, cetTimezone)

			event.Documents = append(event.Documents, structs.FIADocument{
				ID:          utils.Sha256HashToString(documentUrl),
				EventTitle:  eventTitle,
				Title:       title,
				URL:         documentUrl,
				PublishDate: publishDate,
			})
		})

		events = append(events, event)
	})

	unix := time.Now().Unix()

	err := c.Visit(fmt.Sprintf("%s?t=%d", url, unix))
	if err != nil {
		return []structs.FIAEvent{}, err
	}

	return events, nil
}
