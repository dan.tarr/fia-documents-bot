package main

import (
	"fmt"
	"net/http"
	"os"

	"fia-documents-bot/logger"
	"fia-documents-bot/twitter"
)

func UploadPDFImages(twitterHTTPClient *http.Client, i int, mediaIDs *[]string, documentID string) {
	tempDir := os.Getenv("TEMP_DIR")

	media, err := twitter.CreateMediaV1(twitterHTTPClient, fmt.Sprintf("%s/%s/%d.jpeg", tempDir, documentID, i))
	if err != nil {
		logger.Critical("Failed to create Tweet media\n%s", err)
		return
	}

	(*mediaIDs)[i-1] = media.MediaIDString

	logger.Success("Uploaded page %d image to Twitter (Media ID: %s)", i, media.MediaIDString)
}
