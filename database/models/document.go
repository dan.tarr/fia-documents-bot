package models

import (
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
)

type Document struct {
	ID      string
	EventID string
	Season  int
}

func DocumentToMap(document Document) (map[string]types.AttributeValue, error) {
	documentMap, err := attributevalue.MarshalMap(document)
	if err != nil {
		return map[string]types.AttributeValue{}, err
	}

	return documentMap, nil
}

func MapToDocument(mapInput map[string]types.AttributeValue) (Document, error) {
	document := Document{}

	err := attributevalue.UnmarshalMap(mapInput, &document)
	if err != nil {
		return Document{}, err
	}

	return document, nil
}

func MapsToDocuments(mapInputs []map[string]types.AttributeValue) ([]Document, error) {
	documents := make([]Document, 0)

	for _, mapInput := range mapInputs {
		document, err := MapToDocument(mapInput)
		if err != nil {
			return []Document{}, err
		}

		documents = append(documents, document)
	}

	return documents, nil
}
