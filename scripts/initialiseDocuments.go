package main

import (
	"time"

	"fia-documents-bot/aws/dynamodb"
	"fia-documents-bot/database/models"
	"fia-documents-bot/logger"
	"fia-documents-bot/scrapers"
	"fia-documents-bot/utils"

	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	_ "github.com/joho/godotenv/autoload"
)

func initialiseDocuments(formula int) int {
	currentSeason := time.Now().Year()
	documentPage := utils.FindFIADocumentPage(formula, currentSeason)

	if documentPage == nil {
		logger.Critical("Failed to fetch FIA documents URL for F%d %d", formula, currentSeason)
		return 0
	}

	fiaEvents, err := scrapers.ScrapeFIADocuments(documentPage.URL, formula)
	if err != nil {
		logger.Critical("Failed to scrape FIA documents %s", err)
		return 0
	}

	dynamodbClient := dynamodb.CreateClient()

	logger.Info("Adding documents to DynamoDB...")

	totalDocuments := 0

	for _, fiaEvent := range fiaEvents {
		documents := fiaEvent.Documents

		logger.Info("Adding %d documents from \"%s\"", len(documents), fiaEvent.EventTitle)

		for i := 0; i < len(documents); i += 25 {
			var toLoop = 25

			if i+25 > len(documents) {
				toLoop = len(documents) - i
			}

			writeRequests := make([]types.WriteRequest, toLoop)

			for j := 0; j < toLoop; j++ {
				fiaDocument := documents[i+j]
				document := models.Document{
					ID:      fiaDocument.ID,
					EventID: fiaEvent.EventID,
					Season:  currentSeason,
				}

				item, err := models.DocumentToMap(document)
				if err != nil {
					logger.Critical("Failed to convert Document to map\n%s", err)
					return 0
				}

				writeRequests[j] = types.WriteRequest{
					PutRequest: &types.PutRequest{
						Item: item,
					},
				}
			}

			_, err := dynamodb.BatchWriteItem(dynamodbClient, map[string][]types.WriteRequest{
				"FIADocuments": writeRequests,
			})
			if err != nil {
				logger.Critical("oh noo %s", err)
				return 0
			}

			totalDocuments += len(writeRequests)
			logger.Info("Added %d documents to DynamoDB", len(writeRequests))
		}

		time.Sleep(3 * time.Second)
	}

	return totalDocuments
}

func main() {
	logger.Info("Initialising documents...")

	totalDocuments := 0

	for i := 1; i < 4; i++ {
		totalDocuments += initialiseDocuments(i)
	}

	logger.Success("Successfully initialised %d documents", totalDocuments)
}
