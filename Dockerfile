FROM golang:1.17 AS build
LABEL stage=build

WORKDIR /build

COPY . .

RUN go get -d -v
RUN go build -o /fia-documents-bot

FROM ubuntu

# Timezone information required for parsing FIA publish dates
COPY --from=build /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /fia-documents-bot /build/fia-documents-bot

ENTRYPOINT ["/build/fia-documents-bot"]
