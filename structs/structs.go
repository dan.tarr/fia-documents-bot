package structs

import (
	"time"
)

type FIADocumentPage struct {
	Formula int    `json:"formula"`
	Season  int    `json:"season"`
	URL     string `json:"url"`
}

type FIAEvent struct {
	EventID    string
	EventTitle string
	Documents  []FIADocument
}

type FIADocument struct {
	ID          string
	EventTitle  string
	Title       string
	URL         string
	PublishDate time.Time
}

type LambdaEvent struct {
	Formula int `json:"formula"`
}
