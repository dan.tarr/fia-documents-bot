package twitter

import (
	"fmt"
	"os"
)

type Credentials struct {
	ConsumerKey    string
	ConsumerSecret string
	Token          string
	TokenSecret    string
}

func GetCredentials(formula int) Credentials {
	return Credentials{
		ConsumerKey:    os.Getenv(fmt.Sprintf("F%d_TWITTER_CONSUMER_KEY", formula)),
		ConsumerSecret: os.Getenv(fmt.Sprintf("F%d_TWITTER_CONSUMER_SECRET", formula)),
		Token:          os.Getenv(fmt.Sprintf("F%d_TWITTER_ACCESS_TOKEN", formula)),
		TokenSecret:    os.Getenv(fmt.Sprintf("F%d_TWITTER_ACCESS_SECRET", formula)),
	}
}
