package twitter

import (
	"context"
	"net/http"

	"github.com/dghubble/oauth1"
)

func CreateHTTPClient(consumerKey, consumerSecret, token, tokenSecret string) *http.Client {
	oauthConfig := oauth1.NewConfig(consumerKey, consumerSecret)
	oauthToken := oauth1.NewToken(token, tokenSecret)

	httpClient := oauthConfig.Client(context.TODO(), oauthToken)

	return httpClient
}
