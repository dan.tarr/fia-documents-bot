package twitter

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"

	"github.com/google/go-querystring/query"
)

func createMediaV1Request(path string) (*http.Request, error) {
	file, err := os.Open(path)
	if err != nil {
		return &http.Request{}, err
	}

	defer func(file *os.File) {
		_ = file.Close()
	}(file)

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("media", filepath.Base(path))
	if err != nil {
		return &http.Request{}, err
	}

	_, err = io.Copy(part, file)
	if err != nil {
		return &http.Request{}, err
	}

	err = writer.Close()
	if err != nil {
		return &http.Request{}, err
	}

	req, err := http.NewRequest("POST", "https://upload.twitter.com/1.1/media/upload.json", body)
	if err != nil {
		return &http.Request{}, err
	}

	req.Header.Set("Content-Type", writer.FormDataContentType())

	return req, nil
}

func CreateTweet(httpClient *http.Client, body CreateTweetBody) (TweetResponse, error) {
	bodyBytes, err := json.Marshal(body)
	if err != nil {
		return TweetResponse{}, err
	}

	response, err := httpClient.Post("https://api.twitter.com/2/tweets", "application/json", bytes.NewBuffer(bodyBytes))
	if err != nil {
		return TweetResponse{}, err
	}

	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(response.Body)

	responseBody, _ := ioutil.ReadAll(response.Body)

	if response.StatusCode != 200 {
		return TweetResponse{}, errors.New(string(responseBody))
	}

	tweetResponse := TweetResponse{}

	err = json.Unmarshal(responseBody, &tweetResponse)
	if err != nil {
		return TweetResponse{}, err
	}

	return tweetResponse, nil
}

func CreateTweetV1(httpClient *http.Client, body CreateTweetBodyV1) (TweetResponseV1, error) {
	queryString, _ := query.Values(body)

	response, err := httpClient.Post("https://api.twitter.com/1.1/statuses/update.json?"+queryString.Encode(), "application/json", nil)
	if err != nil {
		return TweetResponseV1{}, err
	}

	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(response.Body)

	responseBody, _ := ioutil.ReadAll(response.Body)

	if response.StatusCode != 200 {
		return TweetResponseV1{}, errors.New(string(responseBody))
	}

	tweetResponse := TweetResponseV1{}

	err = json.Unmarshal(responseBody, &tweetResponse)
	if err != nil {
		return TweetResponseV1{}, err
	}

	return tweetResponse, nil
}

func CreateMediaV1(httpClient *http.Client, path string) (MediaResponseV1, error) {
	request, err := createMediaV1Request(path)
	if err != nil {
		return MediaResponseV1{}, err
	}

	response, err := httpClient.Do(request)
	if err != nil {
		return MediaResponseV1{}, err
	}

	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(response.Body)

	responseBody, _ := ioutil.ReadAll(response.Body)

	if response.StatusCode != 200 {
		return MediaResponseV1{}, errors.New(string(responseBody))
	}

	mediaResponse := MediaResponseV1{}

	err = json.Unmarshal(responseBody, &mediaResponse)
	if err != nil {
		return MediaResponseV1{}, err
	}

	return mediaResponse, nil
}
