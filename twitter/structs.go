package twitter

type ReplySettings string

// Enum values for ReplySettings
const (
	ReplySettingsFollowing      ReplySettings = "following"
	ReplySettingsMentionedUsers ReplySettings = "mentionedUsers"
)

// Body types

type CreateTweetBody struct {
	DirectMessageDeepLink string        `json:"direct_message_deep_link"`
	ForSuperFollowersOnly string        `json:"for_super_followers_only"`
	Geo                   Geo           `json:"geo"`
	Media                 Media         `json:"media"`
	Poll                  Poll          `json:"poll"`
	QuoteTweetID          string        `json:"quote_tweet_id"`
	Reply                 Reply         `json:"reply"`
	ReplySettings         ReplySettings `json:"reply_settings"`
	Text                  string        `json:"text"`
}

type CreateTweetBodyV1 struct {
	Status                    string `url:"status,omitempty"`
	InReplyToStatusID         string `url:"in_reply_to_status_id,omitempty"`
	AutoPopulateReplyMetadata bool   `url:"auto_populate_reply_metadata,omitempty"`
	ExcludeReplyUserIDs       string `url:"exclude_reply_user_ids,omitempty"`
	AttachmentURL             string `url:"attachment_url,omitempty"`
	MediaIDs                  string `url:"media_ids,omitempty"`
	PossiblySensitive         bool   `url:"possibly_sensitive,omitempty"`
	Lat                       string `url:"lat,omitempty"`
	Long                      string `url:"long,omitempty"`
	PlaceID                   string `url:"place_id,omitempty"`
	DisplayCoordinates        bool   `url:"display_coordinates,omitempty"`
	TrimUser                  bool   `url:"trim_user,omitempty"`
	EnableDMCommands          bool   `url:"enable_dmcommands,omitempty"`
	FailDMCommands            bool   `url:"fail_dmcommands,omitempty"`
	CardURI                   string `url:"card_uri,omitempty"`
}

type CreateMediaBodyV1 struct {
	Media            []byte `json:"media"`
	MediaCategory    string `json:"media_category"`
	MediaData        string `json:"media_data"`
	AdditionalOwners string `json:"additional_owners"`
}

// Response Types

type TweetResponse struct {
	ID   string `json:"id"`
	Text string `json:"text"`
}

type TweetResponseV1 struct {
	CreatedAt string `json:"created_at"`
	ID        int64  `json:"id"`
	IDString  string `json:"id_str"`
	Text      string `json:"text"`
}

type MediaResponseV1 struct {
	MediaID          int64  `json:"media_id"`
	MediaIDString    string `json:"media_id_string"`
	Size             int    `json:"size"`
	ExpiresAfterSecs int    `json:"expires_after_secs"`
	Image            struct {
		ImageType string `json:"image_type"`
		W         int    `json:"w"`
		H         int    `json:"h"`
	} `json:"image"`
}

// General types

type Geo struct {
	PlaceID string `json:"place_id"`
}

type Media struct {
	MediaIDs      []string `json:"media_ids"`
	TaggedUserIDs []string `json:"tagged_user_ids"`
}

type Poll struct {
	DurationMinutes int      `json:"duration_minutes"`
	Options         []string `json:"options"`
}

type Reply struct {
	ExcludeReplyUserIDs []string `json:"exclude_reply_user_ids"`
	InReplyToTweetID    string   `json:"in_reply_to_tweet_id"`
}
