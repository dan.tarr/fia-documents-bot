package constants

import (
	"fia-documents-bot/structs"
)

var FIADocumentPages = []structs.FIADocumentPage{
	{
		Formula: 1,
		Season:  2021,
		URL:     "https://www.fia.com/documents/season/season-2021-1108/championships/fia-formula-one-world-championship-14",
	},
	{
		Formula: 1,
		Season:  2022,
		URL:     "https://www.fia.com/documents/championships/fia-formula-one-world-championship-14/season/season-2022-2005",
	},
	{
		Formula: 2,
		Season:  2021,
		URL:     "https://www.fia.com/documents/season/season-2021-1108/championships/formula-2-championship-44",
	},
	{
		Formula: 2,
		Season:  2022,
		URL:     "https://www.fia.com/documents/season/season-2022-2005/championships/formula-2-championship-44",
	},
	{
		Formula: 3,
		Season:  2021,
		URL:     "https://www.fia.com/documents/season/season-2021-1108/championships/fia-formula-3-championship-1012",
	},
	{
		Formula: 3,
		Season:  2022,
		URL:     "https://www.fia.com/documents/season/season-2022-2005/championships/fia-formula-3-championship-1012",
	},
}
