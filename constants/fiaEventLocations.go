package constants

var FIAEventTwitterPlaceIDs = map[string]string{
	"Abu Dhabi Grand Prix":                  "00f24ae701a1207b",
	"2021 Etihad Airways Abu Dhabi":         "00f24ae701a1207b",
	"United Arab Emirates Grand prix":       "00f24ae701a1207b",
	"Saudi Arabia Grand Prix":               "000799c66e428a87",
	"Saudi Arabian Grand Prix":              "000799c66e428a87",
	"Qatar Grand Prix":                      "a54c21f6aedb2967",
	"Brazilian Grand Prix":                  "68e019afec7d0ba5",
	"Mexican Grand Prix":                    "25530ba03b7d90c6",
	"United States Grand Prix":              "e0060cda70f5f341",
	"Turkish Grand Prix":                    "4daf916352db96a6",
	"Russian Grand Prix":                    "2716ce1906091cca",
	"Italian Grand Prix":                    "7067a1473fe90df3",
	"Gran Premio d'Italia":                  "7067a1473fe90df3",
	"Dutch Grand Prix":                      "b0edd8b31d86f4d6",
	"Belgian Grand Prix":                    "", // Belgium doesn't seem to have any place IDs on Twitter?
	"Hungarian Grand Prix":                  "81b8dcbe189773f2",
	"Magyar Nagydíj":                        "81b8dcbe189773f2",
	"British Grand Prix":                    "50f2d0272381533f",
	"Formula 1 70th Anniversary Grand Prix": "50f2d0272381533f",
	"Austrian Grand Prix":                   "", // Austria doesn't seem to have any place IDs on Twitter?
	"Grosser Preis von Österreich":          "", // Austria doesn't seem to have any place IDs on Twitter?
	"Styrian Grand Prix":                    "", // Austria doesn't seem to have any place IDs on Twitter?
	"French Grand Prix":                     "1b795f447071784b",
	"Azerbaijan Grand Prix":                 "efc23cd34689b068",
	"Monaco Grand Prix":                     "4df040baa097e37a",
	"Spanish Grand Prix":                    "f3e29c4c744f3625",
	"Portuguese Grand Prix":                 "", // Portugal doesn't seem to have any place IDs on Twitter?
	"Emilia Romagna Grand Prix":             "95237157ed837644",
	"Bahrain Grand Prix":                    "e3e9c55876b99760",
	"Sakhir Grand Prix":                     "e3e9c55876b99760",
	"Eifel Grand Prix":                      "894c8d05cd339ba4",
	"Tuscan Grand Prix":                     "3a642dd4b14ea11c",
	"Australian Grand Prix":                 "01864a8a64df9dc4",
	"Japanese Grand Prix":                   "52eab9df35c1e635",
	"Singapore Grand Prix":                  "2509b9adc1fedfd2",
	"German Grand Prix":                     "b376625558c84057",
	"Canadian Grand Prix":                   "378a442883b05c3c",
	"Chinese Grand Prix":                    "01a20da949498784",
	"Miami Grand Prix":                      "04cb31bae3b3af93",
}
