# FIA Documents Bot

[https://dantarr.dev/p/FIA Documents Bot](https://dantarr.dev/p/FIA%20Documents%20Bot)

A bot to check when new documents are published by the FIA for F1, F2 and F3 and posts them to Twitter.

The bot has been designed around the AWS free tier to allow it to be run for free, permanently.

It runs as a Lambda function which takes in which F_ series to check the documents for.
This function is then invoked by 3 separate EventBridge ScheduledRule's which run every 1 minute, 1 for each F_ series (1, 2 and 3).

DynamoDB is used to store historical documents to track which are new. Each document is stored with a unique document ID composed of hashing the URL of the document
and the event ID, composed of hashing the title of the event.
The event ID is used to only retrieve documents for the latest event when checking for new documents to keep the number of read units required low.

When the function runs and finds no new documents (Will be the majority of the time), it takes approximately 1,200ms from startup to shut down within Lambda.
Ran 3 times per minute with 256MB of allocated RAM, this function will use approximately 39,420 GB-s per month, well within the 400,000 GB-s free tier allowance.

The DynamoDB table is provisioned with 1 read and 1 write unit for each the table and the 1 index (2 units of each total),
which is within the 25 read and write units in the free tier allowance.
According to the DynamoDB items summary, each item is on average 137 bytes.
At an average of 1,100 documents per F1 season, it would take 178,125 seasons to exceed the free tier storage allowance of 25GB.
In other words, storage will always stay free.

Since the Twitter v2 API does not yet support uploading media, access to the v1.1 API (Elevated access) is required.
