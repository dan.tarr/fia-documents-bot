package main

import (
	"fmt"
	"image/jpeg"
	"os"

	"fia-documents-bot/logger"

	"github.com/gen2brain/go-fitz"
)

func CreatePDFImage(doc *fitz.Document, documentID string, i int) {
	tempDir := os.Getenv("TEMP_DIR")

	img, err := doc.Image(i - 1)
	if err != nil {
		logger.Critical("Failed to generate image for page %d\n%s", i, err)
		return
	}

	file, err := os.Create(fmt.Sprintf("%s/%s/%d.jpeg", tempDir, documentID, i))
	if err != nil {
		logger.Critical("Failed to create image file for page %d\n%s", i, err)
		return
	}

	err = jpeg.Encode(file, img, &jpeg.Options{
		Quality: 80,
	})
	if err != nil {
		logger.Critical("Failed to encode image for page %d\n%s", i, err)
		return
	}

	logger.Success("Created page %d image", i)

	_ = file.Close()
}
